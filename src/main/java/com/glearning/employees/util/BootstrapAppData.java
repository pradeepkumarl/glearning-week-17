package com.glearning.employees.util;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.glearning.employees.model.Employee;
import com.glearning.employees.model.Role;
import com.glearning.employees.model.User;
import com.glearning.employees.repository.EmployeeRepository;
import com.glearning.employees.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class BootstrapAppData {
	
	private final EmployeeRepository employeeRepository;
	
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	
	
	@EventListener(ApplicationReadyEvent.class)
	public void insertEmployees(ApplicationReadyEvent event) {
		Employee ramesh = new Employee();
		ramesh.setName("Ramesh");
		ramesh.setEmail("ramesh@gmail.com");
		this.employeeRepository.save(ramesh);
		
		Employee suresh = new Employee();
		suresh.setName("Suresh");
		suresh.setEmail("suresh@gmail.com");

		this.employeeRepository.save(suresh);

		
		User vinay = new User();
		vinay.setUsername("vinay");
		vinay.setPassword(this.passwordEncoder.encode("welcome"));
		vinay.setEmailAddress("vinay@gmail.com");

		Role kiranRole = new Role();
		kiranRole.setRoleName("USER");
		
		Role vinayRole = new Role();
		vinayRole.setRoleName("ADMIN");
		
		vinayRole.setUser(vinay);
		vinay.addRole(vinayRole);
		
		User rakesh = new User();
		rakesh.setUsername("rakesh");
		rakesh.setPassword(this.passwordEncoder.encode("welcome"));
		rakesh.setEmailAddress("rakesh@gmail.com");
		
		Role managerRole = new Role();
		managerRole.setRoleName("MANAGER");
		

		User kiran = new User();
		kiran.setUsername("kiran");
		kiran.setPassword(this.passwordEncoder.encode("welcome"));
		kiran.setEmailAddress("kiran@gmail.com");
		kiran.addRole(kiranRole);
		
		rakesh.addRole(managerRole);
		
		
		
		this.userRepository.save(kiran);
		this.userRepository.save(vinay);
		this.userRepository.save(rakesh);
		
		
	}
}
